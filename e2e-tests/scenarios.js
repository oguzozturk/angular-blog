'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /post_list_category when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/post_list_category");
  });


  describe('post_list_category', function() {

    beforeEach(function() {
      browser.get('index.html#!/post_list_category');
    });


    it('should render post_list_category when user navigates to /post_list_category', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 1/);
    });

  });


  describe('add_post', function() {

    beforeEach(function() {
      browser.get('index.html#!/add_post');
    });


    it('should render add_post when user navigates to /add_post', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 2/);
    });

  });
});

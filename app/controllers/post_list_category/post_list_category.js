'use strict';

angular.module('myApp.post_list_category', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/post_list_category/:id', {
            templateUrl: 'template/post_list_category.html',
            controller: 'CategoryListPostsCtrl'
        });
    }])
    .controller('CategoryListPostsCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
        $scope.category = $routeParams;

        $http.get("http://127.0.0.1:8002/api/posts/")
            .then(function(response) {
                $scope.Posts = response.data;

                $http.get("http://127.0.0.1:8002/api/categories/"+$scope.category.id)
                    .then(function(response) {
                        $scope.category_name = response.data;
                    });
            });

        $http.get("http://127.0.0.1:8002/api/users/")
            .then(function(response) {
                $scope.users = response.data;
            });

    }]);

/**
 * Created by oğuz on 5.8.2016.
 */
'use strict';

angular.module('myApp.see_profile', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/see_profile/', {
            templateUrl: 'template/see_profile.html',
            controller: 'SeeProfileViewCtrl'
        });
    }])

    .controller('SeeProfileViewCtrl', ["$scope", "$http", function ($scope, $http) {

        $scope.is_superuser = window.sessionStorage.getItem("is_superuser");
        $scope.token = window.sessionStorage.getItem("token");
        $scope.username = window.sessionStorage.getItem("username");
        $scope.first_name = window.sessionStorage.getItem("first_name");
        $scope.last_name = window.sessionStorage.getItem("last_name");
        $scope.e_mail = window.sessionStorage.getItem("email");
        $scope.userid = window.sessionStorage.getItem("user");

        $http.get("http://127.0.0.1:8002/api/posts/")
            .then(function (response) {
                $scope.Posts = response.data;
            });

        $http.get("http://127.0.0.1:8002/api/comments/")
            .then(function (response) {
                $scope.Comments = response.data;
            });



    }]);


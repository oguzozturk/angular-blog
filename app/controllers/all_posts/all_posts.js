/**
 * Created by oğuz on 1.8.2016.
 */
'use strict';

angular.module('myApp.all_posts', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/all_posts/', {
            templateUrl: 'template/all_posts.html',
            controller: 'AllPostCtrl'
        });
    }])

    .controller('AllPostCtrl',["$scope","$http", function($scope, $http) {

        $http.get("http://127.0.0.1:8002/api/posts/")
            .then(function(response) {
                $scope.Posts = response.data;
            });

        $http.get("http://127.0.0.1:8002/api/users/")
            .then(function(response) {
                $scope.users = response.data;
            });
    }])
/**
 * Created by oğuz on 1.8.2016.
 */
'use strict';

angular.module('myApp.post_content', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/post_content/:id', {
            templateUrl: 'template/post_content.html',
            controller: 'PostContentCtrl'
        });
    }])

    .controller('PostContentCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
        $scope.post = $routeParams;
        $scope.is_superuser = window.sessionStorage.getItem("is_superuser");
        $scope.token = window.sessionStorage.getItem("token");
        $scope.username = window.sessionStorage.getItem("username");
        $scope.userid = window.sessionStorage.getItem("user");



        $http.get("http://127.0.0.1:8002/api/posts/"+$scope.post.id)
            .then(function(response) {
                $scope.postcont = response.data;

                $http.get("http://127.0.0.1:8002/api/categories/"+$scope.postcont.category)
                    .then(function(response) {
                        $scope.category_name = response.data;
                    });
            });

        $http.get("http://127.0.0.1:8002/api/comments/")
            .then(function(response) {
                $scope.Comments = response.data;
            });

        $http.get("http://127.0.0.1:8002/api/users/")
            .then(function(response) {
                $scope.users = response.data;
            });


        $scope.updatepostcontent = function(postid, post_title, content, like, dislike, category, author) {

            $scope.postid = angular.copy(postid);
            $scope.tit = angular.copy(post_title);
            $scope.cont = angular.copy(content);
            $scope.lik = angular.copy(like);
            $scope.dislik = angular.copy(dislike);
            $scope.cat = angular.copy(category);
            $scope.auth = angular.copy(author);

            $http({
                method: 'PUT',
                url: 'http://127.0.0.1:8002/api/posts/' + $scope.postid + '/',
                data: {
                    post_title: $scope.tit,
                    content: $scope.cont,
                    author: $scope.auth,
                    like: $scope.lik,
                    dislike: $scope.dis,
                    category: $scope.cat,
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
            });
        }

        $scope.deletepostcontent = function(postid) {

            $http({
                method: 'DELETE',
                url: 'http://127.0.0.1:8002/api/posts/' + postid + '/',
                data: {
                    id: postid
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
                window.location = "#!/index";
            });
        }
        
        $scope.createcomment = function (pid,comment) {

            $http({
                method: 'POST',
                url: 'http://127.0.0.1:8002/api/comments/',
                data: {
                    comment_text: comment,
                    like_comment: 0,
                    dislike_comment: 0,
                    post: pid,
                    author: $scope.userid
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
            });

        }

        $scope.updatecommentcontent = function(commentid, like_comment, dislike_comment, author, post, comment_text) {

            $scope.comid = angular.copy(commentid);
            $scope.lik = angular.copy(like_comment);
            $scope.dislk = angular.copy(dislike_comment);
            $scope.auth = angular.copy(author);
            $scope.pst = angular.copy(post);
            $scope.text = angular.copy(comment_text);

            $http({
                method: 'PUT',
                url: 'http://127.0.0.1:8002/api/comments/' + $scope.comid + '/',
                data: {
                    id: $scope.comid,
                    comment_text: $scope.text,
                    like_comment: $scope.lik,
                    dislike_comment: $scope.dislk,
                    post: $scope.pst,
                    author: $scope.auth,
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
            });
        }


        $scope.deletecommentcontent = function(commentid) {

             $scope.comid = angular.copy(commentid);
            $http({
                method: 'DELETE',
                url: 'http://127.0.0.1:8002/api/comments/' + $scope.comid + '/',
                data: {
                    id: $scope.comid
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
            });
        }

        $scope.like = function () {
            $http.get("http://127.0.0.1:8002/api/posts/"+$scope.post.id+"/like")
                .then(function() {
                    location.reload();
                });

        }

        $scope.dislike = function () {
            $http.get("http://127.0.0.1:8002/api/posts/"+$scope.post.id+"/dislike")
                .then(function() {
                    location.reload();
                });

        }

        $scope.comment_like = function (commentid) {
            $http.get("http://127.0.0.1:8002/api/comments/"+commentid+"/like")
                .then(function () {
                    location.reload();
                });
        }

        $scope.comment_dislike = function (commentid) {
            $http.get("http://127.0.0.1:8002/api/comments/"+commentid+"/dislike")
                .then(function () {
                    location.reload();
                });
        }

    }]);
/**
 * Created by oğuz on 2.8.2016.
 */

'use strict';

angular.module('myApp.login', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login/', {
            templateUrl: 'template/login.html',
            controller: 'LoginViewCtrl'
        });
    }])

    .controller('LoginViewCtrl',["$scope","$http","$routeParams", function($scope, $http, $routeParams) {

        $scope.SendData = function(username,password) {
            $scope.un = angular.copy(username);
            $scope.ps = angular.copy(password);


            $http.post("http://127.0.0.1:8002/api/auth/", {username: $scope.un, password: $scope.ps})
                .success(function (data, status, config) {
                    $scope.ResponseDetails = data;
                    sessionStorage.setItem("token", data.token);
                    sessionStorage.setItem("user", data.user);
                    sessionStorage.setItem("username", data.username);
                    sessionStorage.setItem("first_name", data.first_name);
                    sessionStorage.setItem("last_name", data.last_name);
                    sessionStorage.setItem("is_superuser", data.is_superuser);
                    sessionStorage.setItem("email", data.email);
                    location.reload();
                    window.location = "#!/index";
                })
                .error(function (data, status) {
                    if(status == 400){
                        $scope.ResponseDetails = "Response: Invalid JSON";
                    }
                    if(status == 401){
                        $scope.ResponseDetails = "Response: Wrong credentials";
                    }

                    if(status == 404){
                        $scope.ResponseDetails = "Response: Username or password is invalid";
                    }
                });
        }

    }]);

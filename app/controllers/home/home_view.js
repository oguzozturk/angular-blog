/**
 * Created by oğuz on 1.8.2016.
 */
'use strict';

angular.module('myApp.home', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/index/', {
            templateUrl: 'template/home_view.html',
            controller: 'HomeViewCtrl'
        });
    }])

    .controller('HomeViewCtrl', ["$scope", "$http", function ($scope, $http) {

        $scope.is_superuser = window.sessionStorage.getItem("is_superuser");
        $scope.token = window.sessionStorage.getItem("token");
        $scope.username = window.sessionStorage.getItem("username");
        $scope.userid = window.sessionStorage.getItem("user");
        $scope.like_comment = 0;
        $scope.dislike_comment = 0;

        $http.get("http://127.0.0.1:8002/api/categories/")
            .then(function (response) {
                $scope.Categories = response.data;
            });

        $http.get("http://127.0.0.1:8002/api/posts/")
            .then(function (response) {
                $scope.Posts = response.data;
            });

        $scope.updatecategory = function(catid, title) {
            $scope.catid = angular.copy(catid);
            $scope.tit = angular.copy(title);

            $http({
                method: 'PUT',
                url: 'http://127.0.0.1:8002/api/categories/' + $scope.catid + '/',
                data: {
                    title: $scope.tit
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
                window.location = "#!/index";
            });
        }

        $scope.deletecategory = function (catid) {

            $http({
                method: 'DELETE',
                url: 'http://127.0.0.1:8002/api/categories/' + catid+ '/',
                data: {
                    id: catid
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
                window.location = "#!/index";
            });


        }


        $scope.updatepost = function(postid, post_title, content, like, dislike, category, author) {

            $scope.postid = angular.copy(postid);
            $scope.tit = angular.copy(post_title);
            $scope.cont = angular.copy(content);
            $scope.lik = angular.copy(like);
            $scope.dislik = angular.copy(dislike);
            $scope.cat = angular.copy(category);
            $scope.auth = angular.copy(author);

            $http({
                method: 'PUT',
                url: 'http://127.0.0.1:8002/api/posts/' + $scope.postid + '/',
                data: {
                    post_title: $scope.tit,
                    content: $scope.cont,
                    author: $scope.auth,
                    like: $scope.lik,
                    dislike: $scope.dis,
                    category: $scope.cat,
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
                window.location = "#!/index";
            });
        }


        $scope.deletepost = function(postid) {

            $http({
                method: 'DELETE',
                url: 'http://127.0.0.1:8002/api/posts/' + postid + '/',
                data: {
                    id: postid
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
                window.location = "#!/index";
            });
        }


    }]);


/**
 * Created by oğuz on 2.8.2016.
 */
'use strict';

angular.module('myApp.register', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/register/', {
            templateUrl: 'template/register.html',
            controller: 'RegisterViewCtrl'
        });
    }])

    .controller('RegisterViewCtrl', ["$scope", "$http", function ($scope, $http) {


        $scope.sign_up = function (username, password, first_name, last_name, email) {

            $scope.un = angular.copy(username);
            $scope.ps = angular.copy(password);
            $scope.fn = angular.copy(first_name);
            $scope.ln = angular.copy(last_name);
            $scope.em = angular.copy(email);

            $http({
                method: 'POST',
                url: 'http://127.0.0.1:8002/api/users/',
                data: {
                    username: $scope.un,
                    password: $scope.ps,
                    first_name: $scope.fn,
                    last_name: $scope.ln,
                    email: $scope.em,
                    is_active: true,
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function () {
                location.reload();
                window.location = "#!/login";
            });
        }

    }]);
/**
 * Created by oğuz on 4.8.2016.
 */
'use strict';

angular.module('myApp.add_category', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/add_category/', {
            templateUrl: 'template/add_category.html',
            controller: 'AddCategoryViewCtrl'
        });
    }])

    .controller('AddCategoryViewCtrl',["$scope","$http","$routeParams", function($scope, $http, $routeParams) {

        $scope.is_superuser = window.sessionStorage.getItem("is_superuser");

        $scope.createcategory = function(category_title) {

            $scope.category_title = angular.copy(category_title);
            $scope.token = window.sessionStorage.getItem("token");

            $http({
                method: 'POST',
                url: 'http://127.0.0.1:8002/api/categories/',
                data: {
                    title: $scope.category_title,
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                location.reload();
                window.location = "#!/index";
            });
        }

    }]);


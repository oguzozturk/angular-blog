/**
 * Created by oğuz on 2.8.2016.
 */
'use strict';

angular.module('myApp.add_post', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/add_post/', {
            templateUrl: 'template/add_post.html',
            controller: 'AddPostViewCtrl'
        });
    }])

    .controller('AddPostViewCtrl',["$scope","$http","$routeParams", function($scope, $http, $routeParams) {

        $http.get("http://127.0.0.1:8002/api/categories/")
            .then(function (response) {
                $scope.Categories = response.data;
            });

        $scope.createpost = function(catid, post_title, content) {

            $scope.cat_id = angular.copy(catid);
            $scope.tit = angular.copy(post_title);
            $scope.cnt = angular.copy(content);
            $scope.token = window.sessionStorage.getItem("token");
            $scope.userid = window.sessionStorage.getItem("user");
            $scope.lik=0;
            $scope.dis=0;

            $http({
                method: 'POST',
                url: 'http://127.0.0.1:8002/api/posts/',
                data: {
                    category: $scope.cat_id,
                    post_title: $scope.tit,
                    content: $scope.cnt,
                    like: $scope.lik,
                    dislike: $scope.dis,
                    author: $scope.userid
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8',
                    'WWW-Authenticate': 'Token ' + $scope.token
                }
            }).then(function() {
                    location.reload();
                    window.location = "#!/index";
                });
        }

    }]);

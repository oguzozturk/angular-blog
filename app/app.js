'use strict';

// Declare app level module which depends on post_content, and components
var app = angular.module('myApp', [
    'ngRoute',
    'myApp.home',
    'myApp.login',
    'myApp.add_category',
    'myApp.add_post',
    'myApp.all_posts',
    'myApp.other_profiles',
    'myApp.post_content',
    'myApp.post_list_category',
    'myApp.register',
    'myApp.see_profile',
    'myApp.version'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.otherwise({redirectTo: '/index'});
}]).controller('AppCtrl',["$scope","$http", function($scope, $http) {

        $scope.token = window.sessionStorage.getItem("token");
        $scope.username = window.sessionStorage.getItem("username");

        $scope.logout = function () {

            $http({
                method: 'DELETE',
                url: 'http://127.0.0.1:8002/api/auth/',
                data: {
                    username: $scope.username
                },
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            })
                .then(function(response) {
                    console.log(response.data);
                    sessionStorage.clear()
                    location.reload();
                    window.location = "#!/login";
                }, function(rejection) {
                    console.log(rejection.data);
                });
        }

    }]);